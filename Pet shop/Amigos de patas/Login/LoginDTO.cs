﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.Login
{
    class LoginDTO
    {
        public int ID { get; set; }

        public string Usuario { get; set; }

        public string Senha { get; set; }
    }
}
