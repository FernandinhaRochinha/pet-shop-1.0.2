﻿using Amigos_de_patas.RH.cadastros.classes.funcionario;
using Amigos_de_patas.telas.cadastros;
using Amigos_de_patas.telas.cadastros.classes.cadastrar;
using Amigos_de_patas.telas.cadastros.classes.cadastro_de_departamento;
using Amigos_de_patas.telas.Consultas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.RH
{
    public partial class frmAlterar : Form
    {
        public frmAlterar()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            DepartamentoBusiness business = new DepartamentoBusiness();
            List<DepartamentoDTO> lista = business.Listar();

            cbodepartamento.ValueMember = nameof(DepartamentoDTO.ID);
            cbodepartamento.DisplayMember = nameof(DepartamentoDTO.Nome);
            cbodepartamento.DataSource = lista;
        }
        FuncionarioDTO dtos = new FuncionarioDTO();

        public void LoadScreen (Fruncionario_view dto)
        {
            lblid.Text = dto.ID.ToString();
            txtnome.Text = dto.Nome;
            txtcpf.Text = dto.CPF;
            txtcep.Text = dto.CEP;
            txtrg.Text = dto.RG;
            txtidade.Text = dto.Idade.ToString();
            txtsalario.Text = dto.Salario.ToString();
            txtsenha.Text = dto.Senha;
            cbodepartamento.Text = dto.Departamento.ToString();

            
        }

        private void button1_Click(object sender, EventArgs e)
        {

            DepartamentoDTO departamento = cbodepartamento.SelectedItem as DepartamentoDTO;

            this.dtos.ID = int.Parse(lblid.Text.Trim());
            this.dtos.Nome = txtnome.Text.Trim();
            this.dtos.Idade =Convert.ToInt32( txtidade.Text.Trim());
            this.dtos.RG = txtrg.Text.Trim();
            this.dtos.CPF = txtcpf.Text.Trim();
            this.dtos.Senha = txtsenha.Text.Trim();
            this.dtos.CEP = txtcep.Text.Trim();
            this.dtos.Salario = Convert.ToDecimal(txtsalario.Text.Trim());
            this.dtos.Departamento = departamento.ID;
            

            FuncionarioBusiness bs = new FuncionarioBusiness();
            bs.Alterar(dtos);

            MessageBox.Show("Dados do funcionario alterados com sucesso.");

            frmfuncionarios tela = new frmfuncionarios();
            tela.Show();
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmfuncionarios tela = new frmfuncionarios();
            tela.Show();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
        }

        private void cbodepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void frmAlterar_Load(object sender, EventArgs e)
        {

        }
    }
}
