﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.telas.cadastros.classes.cadastro_de_departamento
{
    public class DepartamentoBusiness
    {
        DepartamentoDataBase db = new DepartamentoDataBase();
        public int Salvar(DepartamentoDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("O campo Nome é obrigatório");
            }
            if (dto.Descricao == string.Empty)
            {
                throw new ArgumentException("O campo Descrição é obrigatório");
            }

            return db.Salvar(dto);


        }
        public List<DepartamentoDTO> Listar()
        {
            return db.Listar();
        }
        public List<DepartamentoDTO> Consultar(DepartamentoDTO dto)
        {
            return db.Consultar(dto);
        }

        public void Remover(int ID)
        {
            db.Remover(ID);

        }
        public void Alterar(DepartamentoDTO dto)
        {
            db.Alterar(dto);

        }
    }
}
