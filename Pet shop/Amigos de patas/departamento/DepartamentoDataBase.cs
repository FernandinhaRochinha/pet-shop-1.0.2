﻿using _3_Telas.BásicosCarne;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.telas.cadastros.classes.cadastro_de_departamento
{
    public class DepartamentoDataBase
    {
        public int Salvar(DepartamentoDTO dto)
        {

            string script =
                @" INSERT INTO tb_departamento (nm_departamento , ds_descricao)
                               VALUES (@nm_departamento , @ds_descricao)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_departamento", dto.Nome));
            parms.Add(new MySqlParameter("ds_descricao", dto.Descricao));
            

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);


        }

        public List<DepartamentoDTO> Listar()
        {
            string script = "select * from tb_departamento ";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DepartamentoDTO> lista = new List<DepartamentoDTO>();

            while (reader.Read())

            {
                DepartamentoDTO dto = new DepartamentoDTO();
                dto.ID = reader.GetInt32("id_departamento");
                dto.Nome = reader.GetString("nm_departamento");
                dto.Descricao = reader.GetString("ds_descricao");
              
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
        public List<DepartamentoDTO> Consultar(DepartamentoDTO dto)
        {
            string script =
                @"SELECT * FROM tb_departamento 
                           WHERE nm_departamento like @nm_departamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_departamento", "%" + dto.Nome + "%"));

            Database db = new Database();

            MySqlDataReader r = db.ExecuteSelectScript(script, parms);

            List<DepartamentoDTO> lista = new List<DepartamentoDTO>();
            while (r.Read())
            {
                DepartamentoDTO dtos = new DepartamentoDTO();
                dtos.ID = r.GetInt32("id_funcionario");
                dtos.Nome = r.GetString("nm_funcionario");
                dtos.Descricao = r.GetString("ds_descricao");
                


                lista.Add(dtos);
            }
            r.Close();
            return lista;
        }

        public void Remover(int ID)
        {
            string script = @"DELETE FROM tb_departamento WHERE id_departamento = @id_departamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_departamento", ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(DepartamentoDTO dto)
        {
            string script = @"UPDATE tb_departamento
                                SET nm_departamento = @nm_departamento , 
                                    id_departamento = @id_departamento,
                                    ds_descricao = @ds_descricao 
                                    
                                WHERE id_departamento = @id_departamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_departamento", dto.ID));
            parms.Add(new MySqlParameter("nm_departamento", dto.Nome));
            parms.Add(new MySqlParameter("ds_descricao", dto.Descricao));
            


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }
    }
}
