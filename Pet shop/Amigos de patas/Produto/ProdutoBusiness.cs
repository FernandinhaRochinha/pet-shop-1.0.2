﻿using Amigos_de_patas.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.RH.cadastros.classes.Produto
{
    public class ProdutoBusiness
    {
        ProdutoDatabase db = new ProdutoDatabase();

        public int Salvar(ProdutoDTO dto)
        {
            return db.Salvar(dto);
        }
        public void Remover(int ID)
        {
            db.Remover(ID);
        }
        public List<View_Produto> Listar()
        {
            return db.Listar();
        }
        public List<View_Produto> Consultar(View_Produto dto)
        {
            return db.Consultar(dto);
        }
        public void Alterar(ProdutoDTO dto)
        {
            db.Alterar(dto);
        }

    }
}
