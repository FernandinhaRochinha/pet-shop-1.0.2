﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.Produto
{
    public class View_Produto
    {
        public int ID { get; set; }

        public string Nome { get; set; }

        public decimal Valor { get; set; }

        public decimal Peso { get; set; }

        public string Animal { get; set; }

        public string Marca { get; set; }

        public decimal Idade { get; set; }

        public string Id_Fornecedor { get; set; }
    }
}
