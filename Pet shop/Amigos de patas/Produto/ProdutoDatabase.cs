﻿using _3_Telas.BásicosCarne;
using Amigos_de_patas.Produto;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.RH.cadastros.classes.Produto
{
    public class ProdutoDatabase
    {
        public int Salvar (ProdutoDTO dto)
        {
            string script =
                @" INSERT INTO tb_produto (nm_produto , vl_valor , vl_peso , ds_animal , nm_marca , fk_fornecedor , vl_idade )
                               VALUES (@nm_produto , @vl_valor , @vl_peso , @ds_animal , @nm_marca , @fk_fornecedor , @vl_idade)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("vl_idade", dto.Idade));
            parms.Add(new MySqlParameter("vl_valor", dto.Valor));
            parms.Add(new MySqlParameter("vl_peso", dto.Peso));
            parms.Add(new MySqlParameter("ds_animal", dto.Animal));
            parms.Add(new MySqlParameter("nm_marca", dto.Marca));
            parms.Add(new MySqlParameter("fk_fornecedor", dto.Id_Fornecedor));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int ID)
        {
            string script = @"DELETE FROM tb_produto WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<View_Produto> Listar()
        {
            string script = "select * from view_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<View_Produto> lista = new List<View_Produto>();

            while (reader.Read())

            {
                View_Produto dtos = new View_Produto();
                dtos.ID = reader.GetInt32("id_produto");
                dtos.Nome = reader.GetString("nm_produto");
                dtos.Idade = reader.GetDecimal("vl_idade");
                dtos.Valor = reader.GetDecimal("vl_valor");
                dtos.Peso = reader.GetDecimal("vl_peso");
                dtos.Animal = reader.GetString("ds_animal");
                dtos.Marca = reader.GetString("nm_marca");
                dtos.Id_Fornecedor = reader.GetString("nm_fornecedor");
                



                lista.Add(dtos);
            }
            reader.Close();
            return lista;
        }

        public List<View_Produto> Consultar(View_Produto dto)
        {
            string script =
                @"SELECT * FROM view_produto 
                           WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", "%" + dto.Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<View_Produto> lista = new List<View_Produto>();

            while (reader.Read())

            {
                View_Produto dtos = new View_Produto();
                dtos.ID = reader.GetInt32("id_produto");
                dtos.Nome = reader.GetString("nm_produto");
                dtos.Idade = reader.GetDecimal("vl_idade");
                dtos.Valor = reader.GetDecimal("vl_valor");
                dtos.Peso = reader.GetDecimal("vl_peso");
                dtos.Animal = reader.GetString("ds_animal");
                dtos.Marca = reader.GetString("nm_marca");
                dtos.Id_Fornecedor = reader.GetString("nm_fornecedor");



                lista.Add(dtos);
            }
            reader.Close();
            return lista;
        }

        public void Alterar(ProdutoDTO dto)
        {
            string script = @"UPDATE tb_produto
                                SET 
                                    nm_produto    = @nm_produto ,
                                    vl_idade      = @vl_idade   ,
                                    vl_valor      = @vl_valor   ,
                                    vl_peso       = @vl_peso    ,
                                    ds_animal     = @ds_animal  ,
                                    nm_marca      = @nm_marca   ,
                                    fk_fornecedor = @fk_fornecedor 
                                WHERE id_produto  = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.ID));
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("vl_idade", dto.Idade));
            parms.Add(new MySqlParameter("vl_valor", dto.Valor));
            parms.Add(new MySqlParameter("vl_peso", dto.Peso));
            parms.Add(new MySqlParameter("ds_animal", dto.Animal));
            parms.Add(new MySqlParameter("nm_marca", dto.Marca));
            parms.Add(new MySqlParameter("fk_fornecedor", dto.Id_Fornecedor));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }


    }
}
