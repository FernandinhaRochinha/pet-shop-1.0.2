﻿using Amigos_de_patas.RH.cadastros.classes.Fornecedor;
using Amigos_de_patas.RH.cadastros.classes.funcionario.Fornecedor;
using Amigos_de_patas.RH.cadastros.classes.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.Produto
{
    public partial class frmAlterarProduto : Form
    {
        public frmAlterarProduto()
        {
            InitializeComponent();
        }

        ProdutoDTO dto1;
        public void CarregarCombo()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            cboFornecedor.ValueMember = nameof(FornecedorDTO.ID);
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.Nome);
            cboFornecedor.DataSource = lista;
        }

       public void Loadscreen(View_Produto dto)
        {
           
            txtNome.Text = dto.Nome;
            txtMarca.Text = dto.Marca;
            txtValor.Text = dto.Valor.ToString();
            txtPeso.Text = dto.Peso.ToString();
            txtAnimal.Text = dto.Animal;
            txtIdade.Text = dto.Idade.ToString();
            cboFornecedor.Text = dto.Id_Fornecedor; 
        }

        private void button1_Click(object sender, EventArgs e)
        {

            FornecedorDTO fornecedor = cboFornecedor.SelectedItem as FornecedorDTO;

            
            this.dto1.Nome = txtNome.Text.Trim();
            this.dto1.Marca = txtMarca.Text.Trim();
            this.dto1.Peso = Convert.ToDecimal( txtPeso.Text.Trim());
            this.dto1.Valor = Convert.ToDecimal(txtValor.Text.Trim());
            this.dto1.Idade = Convert.ToDecimal(txtIdade.Text.Trim());
            this.dto1.Animal = txtAnimal.Text.Trim();
            this.dto1.Id_Fornecedor = fornecedor.ID;


            ProdutoBusiness bs = new ProdutoBusiness();
            bs.Alterar(dto1);

            MessageBox.Show("Produto Alterado com sucesso");


        }

        private void frmAlterarProduto_Load(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
    }
}
