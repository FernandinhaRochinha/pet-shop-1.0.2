﻿using Amigos_de_patas.RH.cadastros.classes.Fornecedor;
using Amigos_de_patas.RH.cadastros.classes.funcionario.Fornecedor;
using Amigos_de_patas.telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.RH.cadastros.classes.Produto
{
    public partial class frmProdutoCadastrar : Form
    {
        public frmProdutoCadastrar()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            cboFornecedor.ValueMember = nameof(FornecedorDTO.ID);
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.Nome);
            cboFornecedor.DataSource = lista;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void frmProdutoCadastrar_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FornecedorDTO ID = cboFornecedor.SelectedItem as FornecedorDTO;

            ProdutoDTO dto = new ProdutoDTO();
            dto.Nome = txtNome.Text.Trim();
            dto.Valor = Convert.ToDecimal(txtValor.Text.Trim());
            dto.Peso = Convert.ToDecimal(txtPeso.Text.Trim());
            dto.Marca = txtMarca.Text.Trim();
            dto.Idade = Convert.ToDecimal(txtIdade.Text.Trim());
            dto.Animal = txtAnimal.Text.Trim();
            dto.Id_Fornecedor = ID.ID ;



            ProdutoBusiness bs = new ProdutoBusiness();
            bs.Salvar(dto);

            MessageBox.Show("Produto cadastrado com sucesso", "Amigos de Patas",
                             MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            this.Close();
        }

        private void cboFornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
