﻿using Amigos_de_patas.cliente.Cliente;
using Amigos_de_patas.telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.cliente
{
    public partial class frmcadastro : Form
    {
        public frmcadastro()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Nome = txtNome.Text.Trim();
                dto.Sobrenome = txtSobrenome.Text.Trim();
                dto.Telefone = txtTelefone.Text.Trim();
                dto.RG = txtRG.Text.Trim();
                dto.Animal = txtAnimal.Text.Trim();
                dto.Email = txtEmail.Text.Trim();
                dto.Raca = txtRaca.Text.Trim();

                ClienteBusiness bs = new ClienteBusiness();
                bs.Salvar(dto);

                MessageBox.Show("Cliente cadastrado com sucesso", "Amigos de Patas",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);

                frminicio tela = new frminicio();
                tela.Show();
                this.Close();
            }
            catch (Exception)
            {

                MessageBox.Show("Preencha todos os campos", "Amigos de Patas",
                                 MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            

        }

        private void button4_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            this.Close();
        }
    }
}
