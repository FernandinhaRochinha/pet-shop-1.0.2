﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.cliente.Cliente
{
    public class ClienteDTO
    {
        public int ID { get; set; }

        public string Nome { get; set; }

        public string Sobrenome { get; set; }

        public string Email { get; set; }

        public string RG { get; set; }

        public string Animal { get; set; }

        public string Raca { get; set; }

        public string Telefone { get; set; }

    }
}
