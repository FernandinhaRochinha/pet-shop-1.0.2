﻿using Amigos_de_patas.telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.cliente.Cliente
{
    public partial class frmConsultar : Form
    {
        public frmConsultar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClienteDTO dto = new ClienteDTO();
            dto.Nome = txtbuscar.Text;

            ClienteBusiness bs = new ClienteBusiness();
            List<ClienteDTO> consultar =  bs.Consultar(dto);

            dgvcliente.AutoGenerateColumns = false;
            dgvcliente.DataSource = consultar;
        
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            this.Hide();

        }

        private void dgvcliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                ClienteDTO funcionario = dgvcliente.Rows[e.RowIndex].DataBoundItem as ClienteDTO;

                DialogResult r = MessageBox.Show($"Deseja realmente excluir esse Cliente ?", "Amigos de patas",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ClienteBusiness business = new ClienteBusiness();
                    business.Remover(funcionario.ID);

                    MessageBox.Show("Cliente Removido com exito", "Amigos de Patas"
                                    , MessageBoxButtons.OK, MessageBoxIcon.Information);

                    ClienteBusiness busines = new ClienteBusiness();
                    List<ClienteDTO> listar = busines.Listar();

                    dgvcliente.AutoGenerateColumns = false;
                    dgvcliente.DataSource = listar;
                }
            }
        }
    }
}
