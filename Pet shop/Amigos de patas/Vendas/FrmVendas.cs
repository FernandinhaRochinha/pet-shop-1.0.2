﻿using Amigos_de_patas.cliente.Cliente;
using Amigos_de_patas.DB.VendaItem;
using Amigos_de_patas.DB.Vendas;
using Amigos_de_patas.Produto;
using Amigos_de_patas.RH.cadastros.classes.funcionario;
using Amigos_de_patas.RH.cadastros.classes.Produto;
using Amigos_de_patas.telas;
using Amigos_de_patas.telas.cadastros.classes.cadastrar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.Vendas
{
    public partial class FrmVendas : Form
    {
        BindingList<VendaItemDTO> carrinho = new BindingList<VendaItemDTO>();

        public FrmVendas()
        {
            InitializeComponent();
            CarregarGrid();
            CarregarCombos();
        }
       ClienteDTO cliente = new ClienteDTO();

        void CarregarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = carrinho;


            ClienteBusiness business = new ClienteBusiness();
            dgvCliente.AutoGenerateColumns = false;
            dgvCliente.DataSource = business.Listar();

        }

        void CarregarCombos()
        {
            // Carregar Produto
            ProdutoBusiness business = new ProdutoBusiness();
            List<View_Produto> lista = business.Listar();
            cboProduto.ValueMember = nameof(ProdutoDTO.ID);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Nome);
            cboProduto.DataSource = lista;

            //Carregar Funcionario
            FuncionarioBusiness  busines = new FuncionarioBusiness();
            List<Fruncionario_view> lista2 = busines.Listar();
            cboFuncionario.ValueMember = nameof(Fruncionario_view.ID);
            cboFuncionario.DisplayMember = nameof(Fruncionario_view.Nome);
            cboFuncionario.DataSource = lista2;

        }

       

       
        

        private void dgvItens_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                VendaItemDTO dto = dgvItens.CurrentRow.DataBoundItem as VendaItemDTO;
                DialogResult resp = MessageBox.Show("Deseja realmente excluir?", "Company", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (resp == DialogResult.Yes)
                {
                    carrinho.Remove(dto);
                }
            }
        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            VendaItemDTO dto = cboProduto.SelectedItem as VendaItemDTO;

            for (int i = 0; i < nudQuantidade.Value; i++)
            {
                carrinho.Add(dto);
            }
            CarregarGrid();

        }

        private void dgvCliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ClienteDTO dto = dgvCliente.CurrentRow.DataBoundItem as ClienteDTO;
            this.cliente = dto;
            MessageBox.Show("Cliente Escolhido", "Amigos de Patas", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //FuncionarioDTO dtoFuncionario = cboFuncionario.SelectedItem as FuncionarioDTO;
            //int IDFunc = dtoFuncionario.ID;
            


            //VendasBusiness business = new VendasBusiness();
            //business.Salvar(carrinho.ToList(), cliente, dtoFuncionario);

            //MessageBox.Show("Venda Finalizada", "Company", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //frminicio menu = new frminicio();
            //menu.Show();
            //this.Hide();

        }

        private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {

        }
    }
}

