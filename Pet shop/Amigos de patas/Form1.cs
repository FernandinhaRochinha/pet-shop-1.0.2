﻿using Amigos_de_patas.Login;
using Amigos_de_patas.telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoginDTO dto = new LoginDTO();
            dto.Usuario = txtusuario.Text.Trim();
            dto.Senha = txtsenha.Text.Trim();

            LoginBusiness bs = new LoginBusiness();
            bool login = bs.VerificarRegistro(dto);
            if (login == true )
            {
                frminicio tela = new frminicio();
                tela.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Usuario invalido ou nao existe" , "Amigos de Patas" ,
                                 MessageBoxButtons.OK , MessageBoxIcon.Exclamation);
            }

            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
