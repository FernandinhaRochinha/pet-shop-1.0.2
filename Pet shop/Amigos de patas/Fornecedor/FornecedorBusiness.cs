﻿using Amigos_de_patas.RH.cadastros.classes.funcionario.Fornecedor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.RH.cadastros.classes.Fornecedor
{
    public class FornecedorBusiness
    {
        FornecedorDatabase db = new FornecedorDatabase();

        public int Salvar(FornecedorDTO dto)
        {
            return db.Salvar(dto);
        }
        public List<FornecedorDTO> Listar()
        {
            return db.Listar();
        }
        public void Remover(int ID)
        {
            db.Remover(ID);
        }
        public List<FornecedorDTO> Consultar(FornecedorDTO dto)
        {
            return db.Consultar(dto);
        }
        public void Alterar(FornecedorDTO dto)
        {
            db.Alterar(dto);
        }


    }
}
