﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.RH.cadastros.classes.funcionario.Fornecedor
{
    public class FornecedorDTO
    {
        public int ID { get; set; }

        public string Nome { get; set; }

        public DateTime Entrega { get; set; }

        public string Telefone { get; set; }

        public DateTime Cobranca {get;set;}

        public string Rua { get; set; }

        public string Bairro { get; set; }

        public string UF { get; set; }

        public string Cep { get; set; }

        public string Email { get; set; }

        public string Atividade { get; set; }


    }
}
