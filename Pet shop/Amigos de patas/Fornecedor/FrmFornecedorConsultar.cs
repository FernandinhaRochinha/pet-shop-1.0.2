﻿using Amigos_de_patas.RH.cadastros.classes.funcionario.Fornecedor;
using Amigos_de_patas.telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.RH.cadastros.classes.Fornecedor
{
    public partial class FrmFornecedorConsultar : Form
    {
        public FrmFornecedorConsultar()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            this.Close();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            FornecedorDTO dto = new FornecedorDTO();
             dto.Nome = txtbuscar.Text.Trim();
             FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> consultar = business.Consultar(dto);

            dgvFornecedor.AutoGenerateColumns = false;
            dgvFornecedor.DataSource = consultar;
        }
    }
}
