﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.Estoque
{
    public class Estoque_View
    {
        public int ID { get; set; }

        public string Produto { get; set; }

        public decimal Valor { get; set; }

        public string Fornecedor { get; set; }

        public DateTime Entrada { get; set; }

        public DateTime Vencimento { get; set; }

        public int Quantidade { get; set; }

    }
}
