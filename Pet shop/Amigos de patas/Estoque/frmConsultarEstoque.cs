﻿using Amigos_de_patas.Produto;
using Amigos_de_patas.telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.Estoque
{
    public partial class frmConsultarEstoque : Form
    {
        public frmConsultarEstoque()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Estoque_View dto = new Estoque_View();
            dto.Produto = txtProduto.Text.Trim();

            Estoquebusiness bs = new Estoquebusiness();
            List<Estoque_View> Consultar = bs.Consultar(dto);

            dgvEstoque.AutoGenerateColumns = false;
            dgvEstoque.DataSource = Consultar;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            Close();
        }
    }
}
