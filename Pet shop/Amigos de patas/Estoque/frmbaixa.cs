﻿using Amigos_de_patas.Produto;
using Amigos_de_patas.RH.cadastros.classes.Produto;
using Amigos_de_patas.telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.Estoque
{
    public partial class frmbaixa : Form
    {
        public frmbaixa()
        {
            InitializeComponent();
            CarregarCombo();
        }

        public void CarregarCombo()
        {
            ProdutoBusiness bs = new ProdutoBusiness();
            List<View_Produto> listar = bs.Listar();

            cboProduto.ValueMember = nameof(View_Produto.ID);
            cboProduto.DisplayMember = nameof(View_Produto.Nome);

            cboProduto.DataSource = listar;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            EstoqueDTO dto = new EstoqueDTO();
            View_Produto produto = cboProduto.SelectedItem as View_Produto;

            dto.Id_Produto = produto.ID;
            dto.Quantidade = Convert.ToInt32(nudQuantidade.Text);
            dto.Vencimento = mtdValidade.SelectionStart;
            dto.Entrada = DateTime.Now;
            Estoquebusiness bs = new Estoquebusiness();
            bs.Alterar(dto);

            MessageBox.Show("Baixa efetuada com sucesso");
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            Close();
        }
    }
}
