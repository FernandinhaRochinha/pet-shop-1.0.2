﻿using _3_Telas.BásicosCarne;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.Estoque
{
    public class EstoqueDatabase
    {

        public int Salvar (EstoqueDTO dto)
        {
            string script = @"INSERT INTO tb_estoque (
                                     id_produto,
                                     dt_entrada,
                                     dt_vencimento,
                                     vl_quantidade   )
                              VALUES                 (
                                     @id_produto,
                                     @dt_entrada,
                                     @dt_vencimento,
                                     @vl_quantidade   )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.Id_Produto));
            parms.Add(new MySqlParameter("dt_entrada", dto.Entrada));
            parms.Add(new MySqlParameter("dt_vencimento", dto.Vencimento));
            parms.Add(new MySqlParameter("vl_quantidade", dto.Quantidade));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
        public void Remover(int ID)
        {
            string script = @"DELETE FROM tb_estoque WHERE id_estoque = @id_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(EstoqueDTO dto)
        {
            string script = @"UPDATE tb_estoque
                                SET  
                                    dt_entrada = @dt_entrada,
                                    dt_vencimento = @dt_vencimento,
                                    vl_quantidade = @vl_quantidade
                                    
                                WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.Id_Produto));
            parms.Add(new MySqlParameter("dt_entrada", dto.Entrada));
            parms.Add(new MySqlParameter("dt_vencimento", dto.Vencimento));
            parms.Add(new MySqlParameter("vl_quantidade", dto.Quantidade));




            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }

        public List<Estoque_View> Listar()
        {
            string script = "select * from view_estoque ";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Estoque_View> lista = new List<Estoque_View>();

            while (reader.Read())

            {
                Estoque_View dto = new Estoque_View();
                dto.ID = reader.GetInt32("id_estoque");
                dto.Produto = reader.GetString("nm_produto");
                dto.Valor = reader.GetDecimal("vl_valor");
                dto.Vencimento = reader.GetDateTime("dt_vencimento");
                dto.Entrada = reader.GetDateTime("dt_entrada");
                dto.Fornecedor = reader.GetString("nm_fornecedor");
                dto.Quantidade = reader.GetInt32("vl_quantidade");


                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
        public List<Estoque_View> Consultar(Estoque_View dto)
        {
            string script =
                @"SELECT * FROM view_estoque 
                           WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", "%" + dto.Produto + "%"));

            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Estoque_View> lista = new List<Estoque_View>();
            while (reader.Read())
            {
                Estoque_View dtos = new Estoque_View();
                dtos.ID = reader.GetInt32("id_estoque");
                dtos.Produto = reader.GetString("nm_produto");
                dtos.Valor = reader.GetDecimal("vl_valor");
                dtos.Vencimento = reader.GetDateTime("dt_vencimento");
                dtos.Entrada = reader.GetDateTime("dt_entrada");
                dtos.Fornecedor = reader.GetString("nm_fornecedor");
                dtos.Quantidade = reader.GetInt32("vl_quantidade");



                lista.Add(dtos);
            }
            reader.Close();
            return lista;
        }
    }
}
