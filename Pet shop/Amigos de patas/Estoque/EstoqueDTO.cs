﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.Estoque
{
    public class EstoqueDTO
    {
        public int ID { get; set; }

        public int Id_Produto { get; set; }

        public DateTime Entrada { get; set; }

        public DateTime Vencimento { get; set; }

        public int Quantidade { get; set; }

    }
}
