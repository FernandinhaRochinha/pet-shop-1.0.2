﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.Estoque
{
    public class Estoquebusiness
    {
        EstoqueDatabase db = new EstoqueDatabase();

        public int Salvar(EstoqueDTO dto)
        {
            return db.Salvar(dto);
        }
        public void Remover (int ID)
        {
            db.Remover(ID);
        }
        public void Alterar(EstoqueDTO dto)
        {
            db.Alterar(dto);
        }
        public List<Estoque_View> Listar()
        {
            return db.Listar();

        }
        public List<Estoque_View> Consultar(Estoque_View dto)
        {
            return db.Consultar(dto);
        }


    }
}
