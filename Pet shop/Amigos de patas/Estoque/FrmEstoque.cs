﻿using Amigos_de_patas.Produto;
using Amigos_de_patas.RH.cadastros.classes.Produto;
using Amigos_de_patas.telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.Estoque
{
    public partial class FrmEstoque : Form
    {
        public FrmEstoque()
        {
            InitializeComponent();
            CarregarCombo();
        }

        public void CarregarCombo()
        {
            ProdutoBusiness bs = new ProdutoBusiness();
            List<View_Produto> listar = bs.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.ID);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Nome);
            cboProduto.DataSource = listar;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            View_Produto Produto = cboProduto.SelectedItem as View_Produto;

            EstoqueDTO dto = new EstoqueDTO();
            dto.Entrada = DateTime.Now;
            dto.Id_Produto = Produto.ID;
            dto.Quantidade = Convert.ToInt32(nudQuantidade.Value);
            dto.Vencimento = mtdValidade.SelectionStart;

            Estoquebusiness bs = new Estoquebusiness();
            bs.Salvar(dto);

            MessageBox.Show("Baixa efetuada com sucesso!");
        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            Close();
        }
    }
}
