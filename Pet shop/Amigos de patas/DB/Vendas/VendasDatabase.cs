﻿using _3_Telas.BásicosCarne;
using Amigos_de_patas.Vendas;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.DB.Vendas
{
    public class VendasDatabase
    {
        public int Salvar(VendasDTO dto)
        {
            string script = @"Insert into tb_item(
                            id_funcionario,
                            id_produto,
                            id_cliente,
                            dt_compra)

                    VALUES  (
                            @id_funcionario,
                            @id_produto,
                            @id_cliente,
                            @dt_compra     )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.Id_Funcionario));
            parms.Add(new MySqlParameter("id_produto", dto.Id_Produto));
            parms.Add(new MySqlParameter("id_cliente", dto.Id_Cliente));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

    }
}
