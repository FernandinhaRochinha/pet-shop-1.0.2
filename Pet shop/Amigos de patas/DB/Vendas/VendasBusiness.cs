﻿using Amigos_de_patas.Vendas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.DB.Vendas
{
    public class VendasBusiness
    {
        VendasDatabase db = new VendasDatabase();

        public int Salvar (VendasDTO dto)
        {
            return db.Salvar(dto);
        }
    }
}
