﻿using Amigos_de_patas.RH.cadastros.classes.funcionario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.telas.cadastros.classes.cadastrar
{
    public class FuncionarioBusiness
    {
        FuncionarioDataBase db = new FuncionarioDataBase();

        public int Salvar(FuncionarioDTO dto)
        {

            return db.Salvar(dto);


        }
        public List<Fruncionario_view> Listar()
        {
            return db.Listar();
        }
        public List<Fruncionario_view> Consultar(Fruncionario_view dtos)
        {
            return db.Consultar(dtos);
        }

        public void Remover(int ID)
        {
            db.Remover(ID);

        }
        public void Alterar(FuncionarioDTO dto)
        {

            db.Alterar(dto);

        }
    }
        
}
