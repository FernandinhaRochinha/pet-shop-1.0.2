﻿using _3_Telas.BásicosCarne;
using Amigos_de_patas.RH.cadastros.classes.funcionario;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.telas.cadastros.classes.cadastrar
{
    public class FuncionarioDataBase
    {
        public int Salvar(FuncionarioDTO dto)
        {

            string script =
                @" INSERT INTO tb_funcionario (nm_funcionario , vl_idade , ds_cpf , ds_rg , vl_salario , fk_departamento , ds_senha , ds_cep)
                               VALUES (@nm_funcionario , @vl_idade , @ds_cpf , @ds_rg , @vl_salario , @fk_departamento , @ds_senha , @ds_cep)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
            parms.Add(new MySqlParameter("vl_idade", dto.Idade));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("vl_salario", dto.Salario));
            parms.Add(new MySqlParameter("fk_departamento", dto.Departamento));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);


        }

        public List<Fruncionario_view> Listar()
        {
            string script = "select * from view_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Fruncionario_view> lista = new List<Fruncionario_view>();

            while (reader.Read())

            {
                Fruncionario_view dtos = new Fruncionario_view();
                dtos.ID = reader.GetInt32("id_funcionario");
                dtos.Nome = reader.GetString("nm_funcionario");
                dtos.Idade = reader.GetInt32("vl_idade");
                dtos.CPF = reader.GetString("ds_cpf");
                dtos.RG = reader.GetString("ds_rg");
                dtos.Salario = reader.GetDecimal("vl_salario");
                dtos.Departamento = reader.GetString("nm_departamento");
                dtos.Senha = reader.GetString("ds_senha");
                dtos.CEP = reader.GetString("ds_cep");



                lista.Add(dtos);
            }
            reader.Close();
            return lista;
        }
        public List<Fruncionario_view> Consultar(Fruncionario_view dto)
        {
            string script =
                @"SELECT * FROM view_funcionario 
                           WHERE nm_funcionario like @nm_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", "%" + dto.Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Fruncionario_view> lista = new List<Fruncionario_view>();

            while (reader.Read())

            {
                Fruncionario_view dtos = new Fruncionario_view();
                dtos.ID = reader.GetInt32("id_funcionario");
                dtos.Nome = reader.GetString("nm_funcionario");
                dtos.Idade = reader.GetInt32("vl_idade");
                dtos.CPF = reader.GetString("ds_cpf");
                dtos.RG = reader.GetString("ds_rg");
                dtos.Salario = reader.GetDecimal("vl_salario");
                dtos.Departamento = reader.GetString("nm_departamento");
                dtos.Senha = reader.GetString("ds_senha");
                dtos.CEP = reader.GetString("ds_cep");



                lista.Add(dtos);
            }
            reader.Close();
            return lista;
        }

        public void Remover(int ID)
        {
            string script = @"DELETE FROM tb_funcionario WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        

        public void Alterar(FuncionarioDTO dto)
        {
            string script = @"UPDATE tb_funcionario
                                SET
                                    nm_funcionario = @nm_funcionario , 
                                    vl_idade = @vl_idade ,
                                    ds_cpf = @ds_cpf , 
                                    ds_rg = @ds_rg , 
                                    vl_salario = @vl_salario,
                                    fk_departamento = @fk_departamento , 
                                    ds_senha = @ds_senha , 
                                    ds_cep = @ds_cep 
                                WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.ID));
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
            parms.Add(new MySqlParameter("vl_idade", dto.Idade));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("vl_salario", dto.Salario));
            parms.Add(new MySqlParameter("fk_departamento", dto.Departamento));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }
    }
}
