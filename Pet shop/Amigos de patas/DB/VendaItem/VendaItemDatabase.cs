﻿using _3_Telas.BásicosCarne;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.DB.VendaItem
{
    class VendaItemDatabase
    {
        public int Salvar(VendaItemDTO dto)
        {
            string script = @"INSERT INTO tb_venda_itens(
                            id_vendas,
                            id_produto
            VALUES          @id_vendas,
                            @id_produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_vendas", dto.ID_Vendas));
            parms.Add(new MySqlParameter("id_produto", dto.Produto));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
    }
}
