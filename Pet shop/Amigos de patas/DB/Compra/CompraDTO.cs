﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.DB.Compra
{
    class CompraDTO
    {
        public int Id{ get; set; }
        public int Id_Funcionario{ get; set; }
        public int Id_Produto{ get; set; }
        public DateTime Data { get; set; }
    }
}
