﻿using _3_Telas.BásicosCarne;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.DB.Compra
{
    class CompraDatabase
    {
        public int Salvar(CompraDTO dto)
        {

            string script = @"INSERT INTO tb_compra(
                            id_funcionario,
                            id_produto,
                            dt_compra)
                            VALUES(
                        
                            @id_funcionario,
                            @id_produto,
                            @dt_compra)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.Id_Funcionario));
            parms.Add(new MySqlParameter("id_produto", dto.Id_Produto));
            parms.Add(new MySqlParameter("dt_compra", dto.Data));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);



        }

        public void Alterar(CompraDTO dto)
        {
            string script = @"UDATE tb_compra
                            
                            id_funcionario,
                            id_produto,
                            dt_compra

                    SET     
                            @id_funcionario,
                            @id_produto,
                            @dt_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.Id_Funcionario));
            parms.Add(new MySqlParameter("id_produto", dto.Id_Produto));
            parms.Add(new MySqlParameter("dt_compra", dto.Data));

            Database db = new Database();
             db.ExecuteInsertScript(script, parms);
        }

        public void Remover(CompraDTO dto)
        {
            string script = @"DELETE * FROM tb_compra WHERE id_compra =@id_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_compra", dto.Id));

            Database fb = new Database();
            fb.ExecuteInsertScript(script, parms);

        }

        public List<CompraDTO> Listar()
        {

            string script = @"SELECT * FROM tb_compra";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);
            List<CompraDTO> lista = new List<CompraDTO>();
            while(reader.Read())
            {
                CompraDTO dt = new CompraDTO();
                dt.Id = reader.GetInt32("id_compra");
                dt.Id_Funcionario = reader.GetInt32("id_funcionario");
                dt.Id_Produto = reader.GetInt32("id_produto");
                dt.Data= reader.GetDateTime("dt_venda");
                lista.Add(dt);

            }

            reader.Close();
            return lista;
        }

    }
}
