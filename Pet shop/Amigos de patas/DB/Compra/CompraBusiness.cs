﻿using _3_Telas.BásicosCarne;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.DB.Compra
{
    class CompraBusiness
    {
        CompraDatabase db = new CompraDatabase();

        public int Salvar(CompraDTO dto)
        {
            return db.Salvar(dto);
        }
        public void Remover(CompraDTO dto)
        {
            db.Remover(dto);
        }

        public void Alterar(CompraDTO dto)
        {
            db.Alterar(dto);
        }

        public List<CompraDTO> Listar()
        {
            return db.Listar();
        }
    }
}
