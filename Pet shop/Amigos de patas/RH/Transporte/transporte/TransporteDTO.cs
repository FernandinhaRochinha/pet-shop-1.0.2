﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.Transporte.transporte
{
    class TransporteDTO
    {
        public int ID { get; set; }

        public int Cliente { get; set; }

        public string Rua { get; set; }

        public string Bairro { get; set; }

        public string Numero { get; set; }

        public DateTime Horario { get; set; }

        public string Complemento { get; set; }

        public int Funcionario { get; set; }
    }
}
