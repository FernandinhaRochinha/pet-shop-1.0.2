﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.RH.cadastros.classes.Folha_de_pagamento
{
    class RHDTO
    {
        public int id_ponto { get; set; }
        public DateTime Data { get; set; }
        public string hr_entrada { get; set; }
        public string hr_almoco_saida { get; set; }
        public string hr_almoco_retorno { get; set; }
        public string hr_saida { get; set; }
        public int id_funcionario { get; set; }
    }
}
