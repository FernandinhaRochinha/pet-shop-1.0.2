﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.Animal.cadastro
{
    class AnimalDTO
    {
        public int ID { get; set; }

        public int Alojamento { get; set; }

        public string Animal { get; set; }

        public string Raca { get; set; }

        public int Idade { get; set; }
    }
}
