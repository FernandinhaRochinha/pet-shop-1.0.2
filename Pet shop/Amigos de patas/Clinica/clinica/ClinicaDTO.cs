﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.Clinica.clinica
{
    class ClinicaDTO
    {
        public int ID { get; set; }

        public int Funcionario { get; set; }

        public int Animal { get; set; }

        public int Produto { get; set; }

        public string tratamento { get; set; }
    }
}
